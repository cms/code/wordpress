#!/usr/bin/env bash
#
# Name: .sync.sh
# Description: 从网络拉取最新发行版的代码
# Author: Jetsung Chan <jetsungchan@gmail.com>
#
#

set -euo pipefail

# 判断是否已经存在tag
check_tag_exists() {
  _FLAG="${1:-}"
  echo "tag: $_FLAG"

  git fetch origin --tags
  if git tag | grep "$_FLAG"; then
    echo
    echo "$ the tag $_FLAG already exists"
    exit 0
  fi  
}

# 下载并解压
save_and_unzip() {
  echo "downloading $DOWN_URL to $SAVE_ZIP_PATH"
  curl -o "$SAVE_ZIP_PATH" -fsSL "$DOWN_URL"
  if [ ! -f "$SAVE_ZIP_PATH" ]; then
    echo "not found $SAVE_ZIP_PATH"
    exit 1
  fi

  echo "source path: $SOURCE_PATH"
  unzip -o -q "$SAVE_ZIP_PATH" -d "$SAVE_TAG_PATH"
  if [ ! -d "$SAVE_TAG_PATH" ]; then
    echo "not found $SOURCE_PATH"
    exit 1
  fi  
}

# 获取主机名
get_hostname() {
  local url="${1:-}"
  # 提取协议部分
  protocol=$(echo "$url" | sed -E 's#^(https?)://.*#\1#')
  if [ "$protocol" != "http" ] && [ "$protocol" != "https" ]; then
    printf "\n\033[31m[ERROR] protocol is not http or https\033[0m\n"
    exit 0
  fi

  # 提取 hostname 部分
  hostname=$(echo "$url" | sed -E 's#^https?://([^/]+).*#\1#')
  if [ -z "$hostname" ]; then
    printf "\n\033[31m[ERROR] hostname is empty\033[0m\n"
    exit 0
  fi

  echo "$hostname"  
}

main() {
  if [ -f "./.env" ]; then
    # shellcheck source=/dev/null
    source "./.env"
  fi

  # Git User Name
  if [ -n "${GIT_USERNAME:-}" ]; then
    USER_NAME="${GIT_USERNAME:-}" 
  fi
  # From GitLab
  if [ -z "${USER_NAME:-}" ]; then
    USER_NAME="${GITLAB_USER_LOGIN:-}"  
  fi

  # Git User Token
  if [ -n "${GIT_USER_TOKEN:-}" ]; then
    USER_TOKEN="${GIT_USER_TOKEN:-}"
  fi

  # Git User Email
  if [ -n "${GIT_USER_EMAIL:-}" ]; then
    USER_EMAIL="${GIT_USER_EMAIL:-}"
  fi
  # From GitLab
  if [ -z "${USER_EMAIL:-}" ]; then
    USER_EMAIL="${GITLAB_USER_EMAIL:-}"
  fi  

  # 项目地址
  if [ -z "${PROJECT_URL:-}" ]; then
    PROJECT_URL="${CI_PROJECT_URL:-}.git"
  fi

  # Git Hostname
  if [ -n "${GIT_HOSTNAME:-}" ]; then
    SERVER_HOST="${GIT_HOSTNAME:-}"
  fi
  # From GitLab
  if [ -z "${SERVER_HOST:-}" ]; then
    SERVER_HOST="${CI_SERVER_HOST:-}"
  fi
  # From REPO
  if [ -z "${SERVER_HOST:-}" ]; then
    SERVER_HOST="$(get_hostname "${CI_PROJECT_URL:-}")"
  fi

  if [ -z "${USER_NAME:-}" ] || [ -z "${USER_TOKEN:-}" ] || [ -z "${USER_EMAIL:-}" ] || [ -z "${SERVER_HOST:-}" ] || [ -z "${PROJECT_URL:-}" ]; then
    echo "Please set the following environment variables: USER_NAME, USER_TOKEN, USER_EMAIL, SERVER_HOST, PROJECT_URL"
    exit 1
  fi

  echo "USER_NAME: $USER_NAME"
  echo "USER_TOKEN: $USER_TOKEN"
  echo "USER_EMAIL: $USER_EMAIL"
  echo "SERVER_HOST: $SERVER_HOST"
  echo "PROJECT_URL: $PROJECT_URL"  

  SOURCE_WEB_URL="https://wordpress.org"
  SOURCE_WEB_HTML=$(curl -fsL "$SOURCE_WEB_URL/download/")

  # 保存目录
  TEMP_PATH="$(mktemp -d -t git.clone_XXXXXX)"

  # 保存路径
  SAVE_ZIP_PATH="$TEMP_PATH/source.zip"

  # https://wordpress.org/latest.zip
  DOWN_URL="$SOURCE_WEB_URL/latest.zip"
  TAG_FLAG=$(echo "$SOURCE_WEB_HTML" | sed -n '/softwareVersion/p' | cut -d '"' -f 4)

  if [ -z "$TAG_FLAG" ]; then
    echo "not found tag"
    exit 1
  fi

  # 源码目录
  SAVE_TAG_PATH="$TEMP_PATH/$TAG_FLAG"
  SOURCE_PATH="$SAVE_TAG_PATH/wordpress"

  # 判断是否已经存在tag
  check_tag_exists "$TAG_FLAG"

  # 下载并解压
  save_and_unzip

  # 临时分支
  BRANCH_TEMP="temp"

  # 同步代码
  pushd "$SOURCE_PATH" > /dev/null 2>&1 || exit 1
    git config --global init.defaultBranch main
    git config --global pull.rebase false

    git config --global user.email "$USER_EMAIL"
    git config --global user.name "$USER_NAME"
    # git config --list

    git config --global url."https://$USER_NAME:$USER_TOKEN@$SERVER_HOST".insteadOf https://"$SERVER_HOST"

    git init
    git remote add origin "$PROJECT_URL"

    # temp branch
    git checkout -b "$BRANCH_TEMP"
    git add .
    git commit -am "release $TAG_FLAG"
    git fetch

    # 存在分支则切换并合并
    if git branch -a | grep develop; then
      git fetch origin develop
      git checkout develop
      
      # 合并文件
      git checkout "$BRANCH_TEMP" -- .
      
      git add .
      git commit -am "merge $TAG_FLAG"
    else # 不存在分支则创建分支
      git branch -m develop
    fi

    # 推送分支 develop
    git push origin develop

    # 推送标签
    git tag "$TAG_FLAG"
    git push origin "$TAG_FLAG"
  popd > /dev/null 2>&1
}

main "$@" || exit 1